const express = require("express");
const router = express.Router();
const { signup, signin } = require("../handlers/auth");

router.post("/signup48", signup);
router.post("/signin48", signin);

module.exports = router;