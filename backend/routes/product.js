const express = require("express");
const router = express.Router();
const {createProduct, getProduct,showProduct, editProduct, deleteProduct, createImage} = require("../handlers/products");
const { upload} = require('../uploadcontroller')



router.get("/urunler", getProduct );
router.get("/urunler/:id", showProduct);
router.post("/admin/urunler875548674", upload.single('image'), createProduct);
router.put("/admin/urunler875548674/:id", editProduct);
router.delete("/admin/urunler875548674/:id", deleteProduct);
//router.post("/fileupload", createImage);

module.exports = router;