const express = require("express");
const { getManufacture, createManufacture, editManufacture, deleteManufacture, showManufacture } = require("../handlers/manufactures");
const router = express.Router();
const { upload } = require('../uploadcontroller')


router.get("/uretim", getManufacture);
router.get("/uretim/:id", showManufacture);
router.post("/admin/uretim875548674",upload.single('image'), createManufacture);
router.put("/admin/uretim875548674/:id", editManufacture);
router.delete("/admin/uretim875548674/:id", deleteManufacture)

module.exports = router;