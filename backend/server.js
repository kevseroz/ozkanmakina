require("dotenv").config();
const express = require('express');
const bodyParser = require('body-parser');
const path = require("path");



const cors = require('cors');
const errorHandler = require('./handlers/error')
const authRoutes = require("./routes/auth");
const productRoutes = require("./routes/product");
const manufactureRoutes = require("./routes/manufacture");


const app = express();
const PORT = process.env.PORT || 5000;
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use( express.static(__dirname + '/public/uploads'));


app.use("/api/auth", authRoutes);
app.use("/api",  productRoutes );
app.use("/api", manufactureRoutes);

app.use(function(req, res, next) {
    let err = new Error("Not Found");
    err.status = 404;
    next(err);
});



 

    if (process.env.NODE_ENV === 'production') {
      app.use(express.static(path.join(__dirname , '../frontend/build')));

      app.get("/*", (req, res) => {
        res.sendFile(path.join(__dirname, "../frontend/build", "index.html"));
    });
    }

    app.use(errorHandler);

app.listen(PORT, () => console.log(
    `Example app listening at http://localhost:${PORT}`,
  ));
