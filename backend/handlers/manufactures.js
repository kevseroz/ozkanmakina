const db = require('../models');



exports.getManufacture = async function(req, res, next) {
    try{
        const manufactures = await db.Manufacture.find({});
        res.send(manufactures);
    } catch (e) {
        res.status(400).json({msg: e.message})
    }
};

exports.showManufacture = async function(req, res, next) {
    const manufacture = await db.Manufacture.findOne({_id: req.params.id});
    if(manufacture) {
        res.send(manufacture);
    } else {
        res.status(404).send({message: "Üretim Bulunamadı."});
    }
};

exports.createManufacture = async function(req, res, next) {
    try {
        let manufacture = new db.Manufacture({
            name: req.body.name,
            image: req.file.filename,
            description: req.body.description
        });
        const newManufacture = await manufacture.save();
        if(newManufacture) {
           return res.status(201).send({
                message: 'Yeni Üretim Oluşturuldu',
                data: newManufacture
            })
        }            
    } catch (err){
        return next(err);
    }
};

exports.editManufacture = async function(req, res, next) {
    const manufactureId = req.params.id;
    const manufacture =await db.Manufacture.findById(manufactureId);
    if(manufacture){
        manufacture.name = req.body.name;
        manufacture.image = req.file.image;
        manufacture.description = req.body.description;
        const updatedManufacture = await manufacture.save();
        if(updatedManufacture) {
            return res.status(200).send({ 
                message: 'Üretim güncellendi.',
                //buradan bilgi frontende gönderiliyor
                data: updatedManufacture            
            })
        }
    }
    return res.status(500).send({ message: 'Üretim güncellenirken bir hata oluştu'})
};

exports.deleteManufacture = async function(req, res, next) {
    const deletedManufacture = await db.Manufacture.findById(req.params.id);
    if(deletedManufacture) {
        await deletedManufacture.remove();
        res.send({ message: "Üretim başarıyla silindi."});
    } else {
        res.send("Silme işlemi yapılırken bir hata oluştu");
    }
}