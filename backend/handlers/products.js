const db = require('../models');




exports.getProduct = async function(req, res, next) {
    try{
        const products = await db.Product.find({});
        res.send(products);
    } catch (e) {
        res.status(400).json({msg: e.message})
    }
};

exports.showProduct = async function(req, res, next) {
    const product = await db.Product.findOne({_id: req.params.id});
    if(product){
        res.send(product);
    } else {
        res.status(404).send({message: "Ürün bulunamadı."})
    }
};

exports.createProduct =  async function(req, res, next) {
    try {
        let product = new db.Product({
            name: req.body.name,
            image: req.file.filename,
            description: req.body.description
        });
        const newProduct = await product.save();
        if(newProduct) {
           return res.status(201).send({
                message: 'Yeni Ürün Oluşturuldu',
                data: newProduct
            })
        }            
    } catch (err){
        return res.status(500).json(err)
        console.log(err) 
        //next(err);
    }
};

exports.editProduct = async function(req, res, next) {
    const productId = req.params.id;
    const product =await db.Product.findById(productId);
    if(product){
        product.name = req.body.name;
        product.image = req.file.image;
        product.description = req.body.description;
        const updatedProduct = await product.save();
        if(updatedProduct) {
            return res.status(200).send({ 
                message: 'Ürün güncellendi.',
                //buradan bilgi frontende gönderiliyor
                data: updatedProduct            
            })
        }
    }
    return res.status(500).send({ message: 'Ürün güncellenirken bir hata oluştu'})
};

exports.deleteProduct = async function(req, res, next){
    const deletedProduct = await db.Product.findById(req.params.id);
    if(deletedProduct){
        await deletedProduct.remove();
        res.send({ message: "Ürün başarıyla silindi."});
    } else {
        res.send("Silme işlemi yapılırken bir hata oluştu.")
    }
};

/*exports.createImage = async function(req, res, next) {
    if(req.files === null) {
        return res.status(400).json({msg: 'Dosya yüklenmedi.'})
    } 
    const file = req.files.file;

    file.mv(`${__dirname}/frontend/public/uploads/${file.name}`, err => {
        if(err) {
            console.log(err)
            return res.status(500).send(err)
        }

        res.json({fileName: file.name, filePath: `/uploads/${file.name}` })
    })
}*/