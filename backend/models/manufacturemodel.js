const mongoose = require('mongoose');

const ManufactureSchema = new mongoose.Schema({
    name: { 
        type: String, 
        required: true
    },
    image: {
         type: String
        },
    description: { 
        type: String, 
        required: true
    }
})

const Manufacture = mongoose.model("Manufacture", ManufactureSchema);

module.exports =  Manufacture