const mongoose = require('mongoose')


const ProductSchema = new mongoose.Schema({
  //  _id: mongoose.Schema.Types.ObjectId,
    name: { 
        type: String, 
        required: true
    },
    image: {
         type: String, 
        
         
        },
    description: { 
        type: String, 
        required: true
    },
    

}
);

const Product = mongoose.model("Product", ProductSchema);

module.exports = Product