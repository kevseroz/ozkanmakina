const mongoose = require('mongoose');
require('dotenv').config();
mongoose.set("debug", true);
mongoose.Promise = Promise;
mongoose.connect(process.env.MONGODB_URL, {
    autoIndex: false,
    keepAlive: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
});

module.exports.User = require("./usermodel");
module.exports.Product = require("./productmodel");
module.exports.Manufacture = require("./manufacturemodel");