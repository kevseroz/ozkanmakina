import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { detailsManufacture } from './actions/manufacturesactions';
import { Button, Image } from 'react-bootstrap';
import './styles/details.css'
import './styles/card.css'


function ManufactureDetail(props) {


    const manufactureDetails = useSelector(state => state.manufactureDetails);
    const { manufacture, loading, error } = manufactureDetails;
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(detailsManufacture(props.match.params.id));
        return () => {
            //
        }
    }, [])
    return(

        <div className="container" >
        
            {loading? <div>Loading...</div>:
             error? <div>{error}</div>:
             (
                <div>
                <div>
                    <Image rounded thumbnail className="details-image" src={manufacture.image} alt={manufacture.name} />
                </div>
                <div>
                    <h2 className="details-header">{manufacture.name}</h2>
                    <div className="details-desc">
                         {manufacture.description}
                    </div>
                </div>
            </div>
             )
            }
            <Link to="/uretim" >
                <Button className="button type1 details-link">Geri Dön</Button>
            </Link>

        </div>
    )}

 export default ManufactureDetail;