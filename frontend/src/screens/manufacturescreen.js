import React , {useEffect, useState } from 'react'
import {Link, Route, withRouter} from 'react-router-dom'
import { useSelector, useDispatch} from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import  Button  from '@material-ui/core/Button'
import { listManufactures, saveManufacture, deleteManufacture } from '../actions/manufacturesactions';

const useStyles = makeStyles({
    form: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
    },
    formContainer: {
        display: "flex",
        flexDirection: "column",
        width: "32rem",
        padding: "2rem",
        border: ".1rem #f0f0f0 solid",
        borderRadius: ".5rem",
        listStyleType: "none",
        "& li": {
            display:"flex",
            flexDirection: "column",
            marginBottom: "1rem",
        },
    },
    inputBox: {
        padding: "1rem",
        border: ".1rem #c0c0c0 solid",
    }
  });

function ManufactureScreen(props) {

    const classes = useStyles();

    const [modalVisible, setModalVisible] = useState(false);
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [image, setImage] = useState([]);
    const [description, setDescription] = useState('');

    const manufactureList = useSelector(state => state.manufactureList);
    const {loading, manufactures, error} = manufactureList;

    const manufactureSave = useSelector(state => state.manufactureSave);
    const { loading: loadingSave, success: successSave, 
        error: errorSave } = manufactureSave;

    const manufactureDelete = useSelector(state => state.manufactureDelete);
    const {loading: loadingDelete, success: successDelete,
        error: errorDelete} = manufactureDelete;
    
    const dispatch = useDispatch();

    useEffect(() => {
        if(successSave) {
           setModalVisible(false);
       }
        dispatch(listManufactures());
        return () => {

        };
        //bu array içindeki işlemlerden herhangi biri gerçekleşirse sayfa yenileniyor.
    }, [successSave, successDelete]);

    const openModal = (manufacture) => {
        setModalVisible(true)
        setId(manufacture._id);
        setName(manufacture.name);
        setImage(manufacture.image);
        setDescription(manufacture.description);
    }
    
    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(saveManufacture({ _id: id, name, image, description}));
    }

    const deleteHandler = (manufacture) => {
        dispatch(deleteManufacture(manufacture._id))
    }

    return <div className={classes.content, classes.contentMargined}>

    <div className={classes.productHeader}>
        <h3>Ürün Listesi</h3>
         
        <Button 
        variant= "contained"
         color="primary"
         //burada boş obje vererek ürün oluştururken formun boş gelmesini sağlarız.
         onClick={() => openModal({})}
         >Üretim Oluştur</Button>
    </div>
    {modalVisible &&
     <div className={classes.form}>
    <form onSubmit={submitHandler}>
        <ul className={classes.formContainer}>
            <li>
                <h3>
                    Üretim Oluştur
                </h3>
            </li>
            <li>
                {loadingSave && <div>Yükleniyor...</div>}
                {errorSave && <div>{errorSave}</div>}
            </li>
            <li>
                <label 
                htmlFor="name">
                    Üretim Adı
                    </label>
                <input
                className={classes.inputBox}
                 type="text" 
                 name="name" 
                 //value is for edit
                // value={name}
                 id="name" 
                 onChange={(e) => setName(e.target.value)}
                  />
            </li>
            <li>
                <label 
                htmlFor  = "image">
                    Üretim Resmi
                    </label>
                <input
                className={classes.inputBox}
                 type = "file" 
                 name = "image"
                 id = "image" 
                 onChange = {(e) => setImage(e.target.files[0])} />
            </li>
            <li>
                <label 
                htmlFor  = "description">
                    Üretim Açıklaması
                    </label>
                <textarea
                className={classes.inputBox} 
                 name = "description" 
                 value = {description}
                 id = "description" 
                 onChange = {(e) => setDescription(e.target.value)} />
            </li>
            <li>
                <Button 
                variant="contained"
                color="primary"
                type="submit" 
                className="button">
                    {id ? "Güncelle" :"Üretim Oluştur"}
                   
                    </Button>
            </li>
            <li>
                     <Button 
                     variant="contained"
                     color="primary"
                     type="submit" 
                     className="button"
                     onClick={() => setModalVisible(false)} >
                        Gizle
                         </Button>
                 </li>
            </ul>
         </form>
     </div>
}
     <div className={classes.manufactureList}>
            <table>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Üretim İsmi</th>
                        <th>Üretim Resmi</th>
                        <th>Üretim Açıklaması</th>
                    </tr>
                </thead>
                <tbody>
                    { manufactures.map(manufacture =>(
                         <tr key={manufacture._id}>
                             <td>{manufacture._id}</td>
                             <td>{manufacture.name}</td>
                             <td>{manufacture.image}</td>
                             <td>{manufacture.description}</td>
                             <td>
                                 <Button 
                                 variant= "contained" 
                                 color="primary"
                                 //burada fonksiyon editleyeceğimiz ürün formu içi dolu olarak gelir.
                                 onClick={() => openModal(manufacture)}
                                 >Güncelle</Button>
                                 
                                 <Button 
                                 variant= "contained" 
                                 color="primary"
                                 onClick={() => deleteHandler(manufacture)}
                                 >Sil</Button>
                             </td>
                        </tr>
                    ))}
                  
                </tbody>
            </table>
        </div>   
    </div>

}

export default withRouter(ManufactureScreen)