import React , {useEffect, useState } from 'react'
import {Link, Route, withRouter} from 'react-router-dom'
import { useSelector, useDispatch} from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import  Button  from '@material-ui/core/Button'
import { listProducts, saveProduct, deleteProduct } from '../actions/productactions';



const useStyles = makeStyles({
    form: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
    },
    formContainer: {
        display: "flex",
        flexDirection: "column",
        width: "32rem",
        padding: "2rem",
        border: ".1rem #f0f0f0 solid",
        borderRadius: ".5rem",
        listStyleType: "none",
        "& li": {
            display:"flex",
            flexDirection: "column",
            marginBottom: "1rem",
        },
    },
    inputBox: {
        padding: "1rem",
        border: ".1rem #c0c0c0 solid",
    }
  });

function ProductScreen(product) {

    const classes = useStyles();

    const [modalVisible, setModalVisible] = useState(false);
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [image, setImage] = useState([]);
    const [description, setDescription] = useState('');

    const productList = useSelector(state => state.productList);
    const {loading, products, error} = productList;

    const productSave = useSelector(state => state.productSave);
    const { loading: loadingSave, success: successSave, 
        error: errorSave } = productSave;
        
    const productDelete = useSelector(state => state.productDelete);
    const {loading: loadingDelete, success: successDelete,
         error: errorDelete} = productDelete;
    
    const dispatch = useDispatch();

    useEffect(() => {
        if(successSave) {
            setModalVisible(false);
        }
        dispatch(listProducts());
        return () => {

        };
        //bu array içindeki işlemlerden herhangi biri gerçekleşirse sayfa yenileniyor.
    }, [successSave, successDelete]);

    //formu açıp kapayacak bir fonksiyon oluşturuyoruz
    const openModal = (product) => {
        setModalVisible(true)
        setId(product._id);
        setName(product.name);
        setImage(product.image);
        setDescription(product.description);
    }

    const submitHandler = (e) => {
        e.preventDefault();

       
        dispatch(saveProduct({ _id: id, name, image, description}));
        console.log(id)
    }

    const deleteHandler = (product) => {
        dispatch(deleteProduct(product._id))
    }

    return <div className={classes.content, classes.contentMargined}>

        <div className={classes.productHeader}>
            <h3>Ürün Listesi</h3>
             
            <Button 
            variant= "contained"
             color="primary"
             //burada boş obje vererek ürün oluştururken formun boş gelmesini sağlarız.
             onClick={() => openModal({})}
             >Ürün Oluştur</Button>
        </div>
        {modalVisible && 
         <div className={classes.form}>
         <form encType="multipart/form-data" onSubmit={submitHandler} >
             <ul className={classes.formContainer}>
                 <li>
                     <h3>
                         Ürün Oluştur
                     </h3>
                 </li>
                 <li>
                     {loadingSave && <div>Yükleniyor...</div>}
                     {errorSave && <div>{errorSave}</div>}
                 </li>
                 <li>
                     <label 
                     htmlFor="name">
                         Ürün Adı
                         </label>
                     <input
                     className={classes.inputBox}
                      type="text" 
                      name="name" 
                     // value={name}
                      id="name" 
                      onChange={(e) => setName(e.target.value)}
                       />
                 </li>
                 <li>
                     <label 
                     htmlFor  = "image">
                         Ürün Resmi
                         </label>
                     <input
                     className={classes.inputBox}
                      type = "file"
                      name = "image" 
                      id = "image" 
                      onChange = {e => setImage(e.target.files[0])} />
                 </li>
                 <li>
                     <label 
                     htmlFor  = "description">
                         Ürün Açıklaması
                         </label>
                     <textarea
                     className={classes.inputBox} 
                      name = "description" 
                      value = {description}
                      id = "description" 
                      onChange = {(e) => setDescription(e.target.value)} />
                 </li>
                 <li>
                     <Button 
                     variant="contained"
                     color="primary"
                     type="submit" 
                     className="button">
                         {id ? "Güncelle" :"Ürün Oluştur"}
                        
                         </Button>
                 </li>
                 <li>
                     <Button 
                     variant="contained"
                     color="primary"
                     type="submit" 
                     className="button"
                     onClick={() => setModalVisible(false)} >
                        Gizle
                         </Button>
                 </li>
             </ul>
         </form>
     </div>}
       

        <div className={classes.productList}>
            <table>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Ürün İsmi</th>
                        <th>Ürün Resmi</th>
                        <th>Ürün Açıklaması</th>
                    </tr>
                </thead>
                <tbody>
                    {products.map(product =>(
                         <tr key={product._id}>
                             <td>{product._id}</td>
                             <td>{product.name}</td>
                             <td>{product.image}</td>
                             <td>{product.description}</td>
                             <td>
                                 <Button 
                                 variant= "contained" 
                                 color="primary"
                                 //burada fonksiyon editleyeceğimiz ürün formu içi dolu olarak gelir.
                                 onClick={() => openModal(product)}
                                 >Güncelle</Button>
                                 
                                 <Button 
                                 variant= "contained" 
                                 color="primary"
                                 onClick={() => deleteHandler(product)}
                                 >Sil</Button>
                             </td>
                        </tr>
                    ))}
                  
                </tbody>
            </table>
        </div>
   
    
    
   
    </div>
}

export default withRouter(ProductScreen)