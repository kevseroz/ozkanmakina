import { MANUFACTURE_DELETE_FAIL, 
    MANUFACTURE_DELETE_REQUEST, 
    MANUFACTURE_DELETE_SUCCESS, 
    MANUFACTURE_DETAILS_FAIL,
    MANUFACTURE_DETAILS_REQUEST,
    MANUFACTURE_DETAILS_SUCCESS,
    MANUFACTURE_LIST_FAIL,
    MANUFACTURE_LIST_REQUEST,
    MANUFACTURE_LIST_SUCCESS,
    MANUFACTURE_SAVE_FAIL,
    MANUFACTURE_SAVE_REQUEST,
    MANUFACTURE_SAVE_SUCCESS,
 } from "../constants/manufactureconstants";


function manufactureListReducer(state = { manufactures: [] }, action ){

switch (action.type){
   case MANUFACTURE_LIST_REQUEST:
       return { loading: true, manufactures: [] };
   case MANUFACTURE_LIST_SUCCESS:
       return { loading: false, manufactures: action.payload };
   case MANUFACTURE_LIST_FAIL:
       return { loading: false, error: action.payload};
   default: 
   return state;
}
}

function manufactureDetailsReducer(state = { manufacture: {} }, action ){

switch (action.type){
   case MANUFACTURE_DETAILS_REQUEST:
       return { loading: true };
   case MANUFACTURE_DETAILS_SUCCESS:
       return { loading: false, manufacture: action.payload };
   case MANUFACTURE_DETAILS_FAIL:
       return { loading: false, error: action.payload};
   default: 
   return state;
  }
}

function manufactureSaveReducer(state = { manufacture: {} }, action ){

    switch (action.type){
        case MANUFACTURE_SAVE_REQUEST:
            return { loading: true };
        case MANUFACTURE_SAVE_SUCCESS:
            return { loading: false, success: true,  manufacture: action.payload };
        case MANUFACTURE_SAVE_FAIL:
            return { loading: false, error: action.payload};
        default: 
        return state;
    }
}

function manufactureDeleteReducer(state = { manufacture: {} }, action ){

    switch (action.type){
        case MANUFACTURE_DELETE_REQUEST:
            return { loading: true };
        case MANUFACTURE_DELETE_SUCCESS:
            return { loading: false, manufacture: action.payload, success: true   };
        case MANUFACTURE_DELETE_FAIL:
            return { loading: false, error: action.payload};
        default: 
        return state;
    }
}

export { manufactureDetailsReducer, manufactureListReducer, manufactureSaveReducer, manufactureDeleteReducer};