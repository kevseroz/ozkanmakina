import { Route, Switch } from 'react-router-dom'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import { makeStyles } from '@material-ui/core/styles';
import 'animate.css'

import Homepage from './homepage';
import Products from './products'
import ProductDetail from './productdetail';
import Manufactures from './manufactures'
import ManufactureDetail from './manufacturedetail'
import AboutUs from './aboutus'
import Footer from './footer'
import Header from './header';
import SignupScreen from './screens/signupscreen'
import SigninScreen from './screens/signinscreen'
import ProductScreen from './screens/productscreen'
import ManufactureScreen from './screens/manufacturescreen'
import ErrorScreen from './screens/errorscreen'
import PrivateRoute from './restrictedRoute';
import store from './store';
import { setAuthorizationToken, setCurrentUser } from "./actions/useractions";
import jwtDecode from "jwt-decode";
import { useSelector, useDispatch} from 'react-redux';
import ScrollToTop from './scrolltotop'



const useStyles = makeStyles({
    root: {
      flexGrow: 1,
    }
});
//const st = store();

if (localStorage.jwtToken) {
  setAuthorizationToken(localStorage.jwtToken);
  // prevent someone from manually tampering with the key of jwtToken in localStorage
  try {
    store.dispatch(setCurrentUser(jwtDecode(localStorage.jwtToken)));
  } catch (e) {
    store.dispatch(setCurrentUser({}));
  }
}

function App() {
  const auth = useSelector(state => state.userSignin);
    const { isAuthenticated } = auth;
  const classes = useStyles();
  return (
    <div className="App">
       <div className={classes.root}>
            <Header />
            <ScrollToTop>
      <Switch>
      <Route exact path="/" component={ ()  => (<Homepage/>) } />
      <PrivateRoute exact path="/admin/urunler875548674" component={() => (<ProductScreen />)} />
      <Route exact path="/signin48" component={() => (isAuthenticated? <Homepage/>:<SigninScreen />)} />
      <PrivateRoute exact path="/admin/uretim875548674" component={() => (<ManufactureScreen />)} />
      <Route exact path="/signup48" component={() => (isAuthenticated? <SignupScreen/>:<SigninScreen />)} />
      <Route exact path="/urunler" component={() => (<Products />)} />
      <Route exact path="/urunler/:id" component={(props) => (<ProductDetail {...props} />)} />
      <Route exact path="/uretim" component={() => (<Manufactures />)} />
      <Route exact path="/uretim/:id" component={(props) => (<ManufactureDetail {...props} />)} />
      <Route exact path="/hakkimizda" component={() => (<AboutUs />)} />
      <Route component={() => (<ErrorScreen />)} />
      </Switch>
      </ScrollToTop>
      <Footer />
        </div>
    </div>
  );
}

export default App;

