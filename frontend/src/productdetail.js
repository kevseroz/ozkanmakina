import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { detailsProduct } from './actions/productactions';
import ScrollToTop from './scrolltotop';
import { Button, Image } from 'react-bootstrap';
import './styles/details.css'
import './styles/card.css'



function ProductDetail(props) {

    const productDetails = useSelector(state => state.productDetails);
    const { product, loading, error } = productDetails;
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(detailsProduct(props.match.params.id));
        return () => {
            //
        }
    }, [])

    return(

        <div className="container" >
            <ScrollToTop />
        
            {loading? <div>Loading...</div>:
             error? <div>{error}</div>:
             (
                <div>
                <div >
                    <Image rounded thumbnail className="details-image" src={product.image} alt={product.name} />
                </div>
                <div>                   
                    <h2 className="details-header">{product.name}</h2>                   
                    <div className="details-desc">
                        {product.description}
                    </div>     
                </div>
            </div>
             )
            }
    
            <Link to="/urunler" >
                <Button className="button type1 details-link">Geri Dön</Button>
            </Link>
            
        </div>
    )}

 export default ProductDetail;