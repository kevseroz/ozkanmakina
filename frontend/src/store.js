import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { productDetailsReducer, productListReducer, productSaveReducer, productDeleteReducer } from './reducers/productreducers';
import { manufactureDeleteReducer, manufactureDetailsReducer, manufactureListReducer, manufactureSaveReducer } from './reducers/manufacturereducers'
import userreducer from './reducers/userreducer'
import  thunk  from 'redux-thunk';

const initialState = {};
const reducer = combineReducers({
    productList: productListReducer,
    productDetails: productDetailsReducer,
    manufactureList: manufactureListReducer,
    manufactureDetails: manufactureDetailsReducer,
    userSignup: userreducer,
    userSignin: userreducer,
    productSave: productSaveReducer,
    productDelete: productDeleteReducer,
    manufactureSave: manufactureSaveReducer,
    manufactureDelete: manufactureDeleteReducer,
})

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, initialState, composeEnhancer(applyMiddleware(thunk)));
export default store;