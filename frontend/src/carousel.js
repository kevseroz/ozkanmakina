import React, { useState } from 'react'
import { Carousel} from 'react-bootstrap'
import carousel1 from './images/carousel1.jpg'
import carousel2 from './images/carousel2.jpg'
import carousel3 from './images/carousel3.jpg'
import {Animated} from 'react-animated-css'
import 'animate.css/animate.css'
import './styles/carousel.css'



function ControlledCarousel() {
    const [index, setIndex] = useState(0);
  
    const handleSelect = (selectedIndex, e) => {
      setIndex(selectedIndex);
    };
  
    return (
      <Carousel indicators={false} controls={false} pause={false} infiniteLoop={true}  activeIndex={index} onSelect={handleSelect} className="carousel-inner" >
        <Carousel.Item className="item active">
        <Animated animationIn="slideInRight">
          <img
            className="d-block w-100"
            src={carousel1}
            alt="First slide"
          />
          </Animated>
          <Carousel.Caption>
          <Animated animationIn="bounceInLeft" animationInDelay="700">
            <h3  className="carousel-header ">Hızlı Çözümler</h3>
            </Animated>
            <Animated animationIn="fadeInUp" animationInDelay="700">
            <p className="carousel-letter">7/24 iletişim sağlayabileceğiniz destek sistemi.</p>
            </Animated>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item className="item">
          <img
            className="d-block w-100"
            src={carousel2}
            alt="Second slide"
          />
  
          <Carousel.Caption>
          <Animated animationIn="bounceInLeft" animationInDelay="700">
            <h3>Kaliteli Hizmet</h3>
            </Animated>
            <Animated animationIn="fadeInUp" animationInDelay="700">
            <p>Tescilli ürünlerimizle kaliteli hizmet sunuyoruz.</p>
            </Animated>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item className="item">
          <img
            className="d-block w-100"
            src={carousel3}
            alt="Third slide"
          />
  
          <Carousel.Caption>
          <Animated animationIn="bounceInLeft" animationInDelay="700">
            <h3>Kişiye Uygun Çözümler</h3>
            </Animated>
            <Animated animationIn="fadeInUp" animationInDelay="700">
            <p>
                Kişiselleştirilmiş ürünlerimizle hizmet sunuyoruz.
            </p>
            </Animated>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
      
    );
  }
  
 export default ControlledCarousel