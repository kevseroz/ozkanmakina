import React, {useEffect} from 'react'
import {Link} from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { listManufactures } from './actions/manufacturesactions';
import {Card, Button, Row, Col, Container } from 'react-bootstrap'



function Manufactures() { 

    const manufactureList = useSelector(state => state.manufactureList);
    const { manufactures, loading, error } = manufactureList;
    const dispatch = useDispatch();

    useEffect(() => {
            dispatch(listManufactures());

        return () => {
        }
    }, [])
    return( loading ? <div>Loading...</div>:
           error ? <div>{error}</div>:
        <Container>
             <Row>
                 <Col>
                  {
                      
                 manufactures.map(manufacture => 
                                      <Card className="card-inline" key={manufacture._id}>
                                      <Card.Body >
                                      <Link   to ={`/uretim/${manufacture._id}`} >
                                        <Card.Img variant="top" className="card-image-size" src = {manufacture.image} alt = {manufacture.name} />
                                      </Link>

                                      
                                      <Card.Title className = "product-name">
                                      <Link   to ={`/uretim/${manufacture._id}`} className="card-name" >{manufacture.name}</Link>
                                      </Card.Title>
                                      <Link   to ={`/uretim/${manufacture._id}`} >
                                             <Button className="button type1" >Detaylar</Button>
                                        </Link>
                                      </Card.Body>
                                  </Card>
                    )}
                    </Col>
                </Row>
        </Container>
    )
}
    


export default Manufactures;