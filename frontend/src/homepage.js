import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import image1 from './images/carousel1.jpg'
import homepage from './images/homepage.jpg'
import homepage1 from './images/homepage1.jpg'
import carousel2 from './images/carousel2.jpg'


import ControlledCarousel from './carousel'

const useStyles = makeStyles({
    root: {
      flexGrow: 1,
      letterSpacing: "0.1em",
      fontSize: "1.2em",
      "& p": {
          marginTop: "25px",
      }
    },
    imageRound: {
        height: "300px",
        width: "300px",
        borderRadius: "50%",
        marginTop: "40px",
    }
});


function Homepage() {
    const classes = useStyles();
    return(
        <div className={classes.root}>
            <ControlledCarousel />
            <Grid container spacing={1}>
            <Grid item xs={12} sm={6}>
                <img src={image1} className={classes.imageRound} />
                <p>7/24 iletişim sağlayabileceğiniz destek sistemi</p>
            </Grid>
            <Grid item xs={12} sm={6}>
                <img src={homepage} className={classes.imageRound} />
                <p>Tescilli ürünlerimizle kaliteli hizmet sunuyoruz</p>
            </Grid>
            <Grid item xs={12} sm={6}>
                <img src={homepage1} className={classes.imageRound} />
                <p>Kişiselleştirilmiş ürünlerimizle hizmet sunuyoruz.</p>
            </Grid>
            <Grid item xs={12} sm={6}>
                <img src={carousel2} className={classes.imageRound} />
                <p>uygun ve pratik çözümler</p>
            </Grid>
            </Grid>
        </div>
    );
}

export default Homepage;