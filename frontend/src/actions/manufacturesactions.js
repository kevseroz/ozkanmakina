import axios from "axios";
import { MANUFACTURE_DELETE_FAIL, 
    MANUFACTURE_DELETE_REQUEST,
    MANUFACTURE_DELETE_SUCCESS,
    MANUFACTURE_DETAILS_FAIL, 
    MANUFACTURE_DETAILS_REQUEST, 
    MANUFACTURE_DETAILS_SUCCESS, 
    MANUFACTURE_LIST_FAIL, 
    MANUFACTURE_LIST_REQUEST, 
    MANUFACTURE_LIST_SUCCESS,
    MANUFACTURE_SAVE_FAIL,
    MANUFACTURE_SAVE_REQUEST,
    MANUFACTURE_SAVE_SUCCESS,
  } from "../constants/manufactureconstants"

const listManufactures = () => async (dispatch) => {
    try {
        dispatch({type: MANUFACTURE_LIST_REQUEST });
        const { data } = await axios.get("/api/uretim");
        dispatch({ type: MANUFACTURE_LIST_SUCCESS, payload: data });
    }
    catch (error) {
        dispatch({ type: MANUFACTURE_LIST_FAIL, payload: error.message });
    }
}

const saveManufacture = (manufacture) => async (dispatch) => {
    const fd = new FormData();
    fd.append('id', manufacture.id);
    fd.append('image', manufacture.image);
    fd.append('name', manufacture.name);
    fd.append('description', manufacture.description);
    console.log(manufacture.image)
    try {
        dispatch({ type: MANUFACTURE_SAVE_REQUEST, payload: fd});
        if(!fd._id){
            const { data } = await axios.post("/api/admin/uretim875548674", fd, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            dispatch({ type: MANUFACTURE_SAVE_SUCCESS, payload: data});
        } else {
            const { data } = await axios.put("/api/admin/uretim875548674/" + fd._id,
             fd, {headers: {
                "Content-Type": "mutipart/form-data"
             }});
             dispatch({ type: MANUFACTURE_SAVE_SUCCESS, payload: data});
        }

    } catch (error) {
        dispatch({ type: MANUFACTURE_SAVE_FAIL, payload: error.message});
    }
}

const detailsManufacture = (manufactureId) => async (dispatch) => {
    try {
        dispatch({type: MANUFACTURE_DETAILS_REQUEST, payload: manufactureId});
        const { data } = await axios.get("/api/uretim/" + manufactureId);
        dispatch({type: MANUFACTURE_DETAILS_SUCCESS, payload: data });
    } catch (error) {
        dispatch({type: MANUFACTURE_DETAILS_FAIL, payload: error.message})
    }
}

const deleteManufacture = (manufactureId) => async (dispatch) => {
    try {
        dispatch({type: MANUFACTURE_DELETE_REQUEST, payload: manufactureId});
        const {data} = await axios.delete("/api/admin/uretim875548674/" + manufactureId, {
            headers: {
                //Authorization: 'Bearer' + userInfo.token
            }
        });
        dispatch({type: MANUFACTURE_DELETE_SUCCESS, payload: data, success: true});
    } catch (error) {
        dispatch({type: MANUFACTURE_DELETE_FAIL, payload: error.message})
    }
}

export { listManufactures, detailsManufacture, saveManufacture, deleteManufacture };