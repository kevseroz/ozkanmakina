import axios from "axios";
import { PRODUCT_DELETE_FAIL,
    PRODUCT_DELETE_REQUEST, 
    PRODUCT_DELETE_SUCCESS, 
    PRODUCT_DETAILS_FAIL, 
    PRODUCT_DETAILS_REQUEST, 
    PRODUCT_DETAILS_SUCCESS, 
    PRODUCT_LIST_FAIL, 
    PRODUCT_LIST_REQUEST, 
    PRODUCT_LIST_SUCCESS,
    PRODUCT_SAVE_FAIL,
    PRODUCT_SAVE_REQUEST,
    PRODUCT_SAVE_SUCCESS
} from "../constants/productconstants"

const listProducts = () => async (dispatch) => {
    try {
        dispatch({type: PRODUCT_LIST_REQUEST });
        const { data } = await axios.get("/api/urunler");
        dispatch({ type: PRODUCT_LIST_SUCCESS, payload: data });
    }
    catch (error) {
        dispatch({ type: PRODUCT_LIST_FAIL, payload: error.message });
    }
}
//append kısmını productscreen içinde hallet
const saveProduct = (product) => async (dispatch) => {//dispatch yanına getState
    const fd = new FormData();
    fd.append('id', product.id)
    fd.append('image', product.image);
    fd.append('name', product.name);
    fd.append('description', product.description);
    
    console.log(product)
    console.log(product.image)
    console.log(product._id)
    try {
        dispatch({type: PRODUCT_SAVE_REQUEST, payload: fd});
       // const {userSignin: {userInfo}} = getState();
       if(!fd._id) {
        const {data} = await axios.post("/api/admin/urunler875548674/", fd, {
            headers: {
            // 'Authorization': 'Bearer ' + userInfo.token
            "Content-Type": "multipart/form-data"
         }
     });
     dispatch({ type: PRODUCT_SAVE_SUCCESS, payload: data});
       } else {
        const {data} = await axios.put("/api/admin/urunler875548674/" + fd._id,
         fd, {headers:{
            // Authorization: 'Bearer ' + userInfo.token
            "Content-Type": "multipart/form-data"

         }
     });
          dispatch({ type: PRODUCT_SAVE_SUCCESS, payload: data});
       }

    } catch (error) {
        dispatch({ type: PRODUCT_SAVE_FAIL, payload: error.message});

    }
}

const detailsProduct = (productId) => async (dispatch) => {
    try {
        dispatch({type: PRODUCT_DETAILS_REQUEST, payload: productId});
        const {data} = await axios.get("/api/urunler/" + productId);
        dispatch({type: PRODUCT_DETAILS_SUCCESS, payload: data});
    } catch (error) {
        dispatch({type: PRODUCT_DETAILS_FAIL, payload: error.message})
    }
}

const deleteProduct = (productId) => async (dispatch) => {
    try {
        dispatch({type: PRODUCT_DELETE_REQUEST, payload: productId});
        const {data} = await axios.delete("/api/admin/urunler875548674/" + productId, {
            headers: {
                //Authorization: 'Bearer' + userInfo.token
            }
        });
        dispatch({type: PRODUCT_DELETE_SUCCESS, payload: data, success: true});
    } catch (error) {
        dispatch({type: PRODUCT_DELETE_FAIL, payload: error.message})
    }
}


export { listProducts, detailsProduct, saveProduct, deleteProduct};