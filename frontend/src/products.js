import React, {useEffect} from 'react'
import {Link} from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { listProducts } from './actions/productactions';
import {Card, Button, CardGroup, Row, Col, Container } from 'react-bootstrap'
import './styles/card.css'



function Products() { 

    //const [products, setProduct] = useState([]) ;
    const productList = useSelector((state) => state.productList);
    const { products, loading, error } = productList;
    const dispatch = useDispatch();

    useEffect(() => {
            dispatch(listProducts());

        return () => {
        }
    }, [])
    return( loading ? <div>Loading...</div>:
           error ? <div>{error}</div>:
        <Container  >
            <Row>
                <Col>
                  {
                 products.map(product => 
                                      <Card className="card-inline" key={product._id}>
                                      <Card.Body >
                                      <Link   to ={`/urunler/${product._id}`} >
                                        <Card.Img variant="top"  src = {product.image} alt = {product.name} className="card-image-size" />
                                      </Link>

                                      
                                      <Card.Title >
                                      <Link   to ={`/urunler/${product._id}`} className = "card-name" >{product.name}</Link>
                                      </Card.Title>
                                         {// <div className = "product-description">{product.description}</div>
}   
                                        <Link   to ={`/urunler/${product._id}`} >
                                             <Button className="button type1" >Detaylar</Button>
                                        </Link>
                                      </Card.Body>
                                  </Card>
                    )}
                </Col>
            </Row>
        </Container>
    )
}
    


export default Products;

